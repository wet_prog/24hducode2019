var mqtt = require('mqtt')
params = require('./parameters.json')
var client  = mqtt.connect(params.server)

client.on('connect', function () {
  client.subscribe('sensor/temp', function (err) {
    if (err) {
      console.log(err)
    }
  })
})

client.on('message', function (topic, message) {
  console.log(topic.toString())
  console.log(message.toString())
})
