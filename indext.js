var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://test.mosquitto.org')
var inscription = 'presence';

client.on('connect', function () {
    client.subscribe(inscription, function (err) {
      if (!err) {
        client.publish(inscription, 'Hello mqtt')
      }
    })
  })
   
  client.on('message', function (topic, message) {
    // message is Buffer
    console.log(message.toString())
    client.end()
  })